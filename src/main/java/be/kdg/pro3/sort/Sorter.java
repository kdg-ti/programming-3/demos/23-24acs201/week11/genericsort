package be.kdg.pro3.sort;

import java.util.List;

public class Sorter {
		private Sorter() {
			//you can't make objects of this class.
		}

		public static <T extends Comparable<T>> void sort(List<T> list) {
			for (int i = 0; i < list.size() - 1; i++) {
				int indexSmallest = i;
				for (int j = i + 1; j < list.size(); j++) {
					if (list.get(j).compareTo(list.get(indexSmallest)) < 0) {
						indexSmallest = j;
					}
				}
				T tmp = list.get(i);
				list.set(i, list.get(indexSmallest));
				list.set(indexSmallest, tmp);
			}
		}

	}
