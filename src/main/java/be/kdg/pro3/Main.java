package be.kdg.pro3;

import be.kdg.pro3.sort.Sorter;

import java.util.*;

public class Main {
	public static void main(String[] args) {
		List<String> words = Arrays.asList("you", "make", "my", "pants", "want", "to", "get", "up",
			"and", "dance");
		Sorter.sort(words);
		System.out.println(words);
		List<Integer> numbers = Arrays.asList(15,8,6,58,3,497,3,1,20);
		Sorter.sort(numbers);
		System.out.println(numbers);
	}
}